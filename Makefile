# Copyright © 2020 Johnothan King. All rights reserved.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

# Variables
CC       := cc
CFLAGS   := -O2 -ffast-math -fomit-frame-pointer -fpic -fno-plt -pipe
CPPFLAGS := -D_FORTIFY_SOURCE=2
WFLAGS   := -Wall -Wextra
LDFLAGS  := -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now

all: clean
	@if [ `uname` = FreeBSD ]; then \
		$(CC) $(CFLAGS) $(CPPFLAGS) $(WFLAGS) $(INCLUDE) -D`uname` -o os-indications os-indications.c $(LDFLAGS) -lefivar -lgeom ;\
	elif [ `uname` = Linux ]; then \
		$(CC) $(CFLAGS) $(CPPFLAGS) $(WFLAGS) $(INCLUDE) -D`uname` -o os-indications os-indications.c $(LDFLAGS) ;\
	else \
		echo "os-indications does not support `uname`!" ;\
		false ;\
	fi
	@strip --strip-unneeded -R .comment -R .gnu.version -R .GCC.command.line -R .note.gnu.gold-version os-indications
	@echo "Successfully built os-indications!"

install:
	@mkdir -p "$(DESTDIR)/sbin" "$(DESTDIR)/usr/share/man/man8"
	@install -Dm0644 os-indications.8 "$(DESTDIR)/usr/share/man/man8"
	@install -Dm0755 os-indications   "$(DESTDIR)/sbin"
	@if [ `uname` = FreeBSD ]; then \
		sed -i '' "/#DEF Linux/,/#ENDEF/d" "$(DESTDIR)/usr/share/man/man8/os-indications.8" ;\
		sed -i '' "/#DEF FreeBSD/d" "$(DESTDIR)/usr/share/man/man8/os-indications.8" ;\
		sed -i '' "/#ENDEF/d"       "$(DESTDIR)/usr/share/man/man8/os-indications.8" ;\
	elif [ `uname` = Linux ]; then \
		sed -i "/#DEF FreeBSD/,/#ENDEF/d" "$(DESTDIR)/usr/share/man/man8/os-indications.8" ;\
		sed -i "/#DEF Linux/d" "$(DESTDIR)/usr/share/man/man8/os-indications.8" ;\
		sed -i "/#ENDEF/d"     "$(DESTDIR)/usr/share/man/man8/os-indications.8" ;\
	fi
	@echo "Successfully installed os-indications!"

# Clean the directory
clean:
	@rm -rf os-indications
	@git gc 2> /dev/null
	@git repack >> /dev/null 2> /dev/null

# Calls clean, then resets the git repo
clobber: clean
	git reset --hard

# Alias for clobber
mrproper: clobber
